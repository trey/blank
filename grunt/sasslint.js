module.exports = {
    files: {
        include: ['src/static/css/**/*.scss'],
        ignore: [
            'src/static/css/vendors/*.scss',
            'src/static/css/base/_reset.scss'
        ]
    }
};
