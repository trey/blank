module.exports = {
    files: {
        expand: true,
        cwd: 'src/static/js',
        src: [
            '**/*.jsx',
            '**/*.js'
        ]
    }
};
