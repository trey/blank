module.exports = {
    files: {
        expand: true,
        cwd: 'src/static/js/jsx',
        src: ['**/*.jsx'],
        dest: 'dist/static/js',
        ext: '.js'
    }
};
